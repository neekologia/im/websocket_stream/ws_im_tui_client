use crossterm::{
	cursor::{Hide, Show},
	event::{self, Event, KeyCode, KeyEvent, KeyModifiers},
	terminal::{EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{
	layout::{Constraint, Layout},
	widgets::{List, Block, Borders},
};
use std::{
	sync::{
		mpsc::{channel, Receiver, Sender},
		Arc, Mutex,
	},
	thread::{self, sleep},
	time::Duration,
};
use tui_textarea::{CursorMove, Input, TextArea};
use websocket::{stream::sync::NetworkStream, sync::Client, Message, OwnedMessage, dataframe::{DataFrame, Opcode}};

struct StringChannel {
	sender: Arc<Sender<String>>,
	receiver: Arc<Mutex<Receiver<String>>>,
}

struct EventChannel {
	sender: Arc<Sender<Event>>,
	receiver: Arc<Mutex<Receiver<Event>>>,
}

impl StringChannel {
	fn new() -> Self {
		let channel = channel::<String>();
		Self {
			sender: Arc::new(channel.0),
			receiver: Arc::new(Mutex::new(channel.1)),
		}
	}
}

impl EventChannel {
	fn new() -> Self {
		let channel = channel::<Event>();
		Self {
			sender: Arc::new(channel.0),
			receiver: Arc::new(Mutex::new(channel.1)),
		}
	}
}

fn update_textarea(textarea: &mut TextArea, key: Event, sm_tx: Arc<Sender<String>>) {
	match key {
		Event::Key(KeyEvent {
			code: KeyCode::Enter,
			..
		}) => {
			let line = &textarea.lines()[0];
			if !line.is_empty() {
				sm_tx.send(line.to_string()).unwrap();
				textarea.select_all();
				textarea.cut();
			}
		}
		Event::Key(KeyEvent {
			code: KeyCode::Left,
			..
		}) => {
			textarea.move_cursor(CursorMove::Back);
		}
		Event::Key(KeyEvent {
			code: KeyCode::Right,
			..
		}) => {
			textarea.move_cursor(CursorMove::Forward);
		}
		_ => {
			let input: Input = key.into();
			textarea.input(input);
		}
	}
}

fn spawn_tui(
	key_rx: Arc<Mutex<Receiver<Event>>>,
	rm_rx: Arc<Mutex<Receiver<String>>>,
	sm_tx: Arc<Sender<String>>,
) {
	crossterm::terminal::enable_raw_mode().unwrap();
	crossterm::execute!(std::io::stdout(), EnterAlternateScreen, Hide).unwrap();
	let backend = ratatui::backend::CrosstermBackend::new(std::io::stdout());
	let mut terminal = ratatui::Terminal::new(backend).unwrap();

	let mut items: Vec<String> = vec![];
	let mut textarea = TextArea::default();
	textarea.set_cursor_line_style(ratatui::style::Style::default());
	textarea.set_block(Block::default().borders(Borders::ALL).title("input"));

	let layout = Layout::vertical([Constraint::Fill(1), Constraint::Length(3)]);
	let mut update = true;
	loop {
		terminal.autoresize().unwrap();
		sleep(Duration::from_millis(10));
		if let Ok(text) = rm_rx.lock().unwrap().try_recv() {
			items.push(text);
			update = true;
		}
		if let Ok(key) = key_rx.lock().unwrap().try_recv() {
			update_textarea(&mut textarea, key, Arc::clone(&sm_tx));
			update = true;
		}
		if !update {
			continue;
		}
		terminal
			.draw(|frame| {
				let area = frame.size();
				let [top, bottom] = layout.areas::<2>(area);
				let items_iter = items.iter().rev().take(top.height as usize).cloned().rev();
				let items_view = items_iter.collect::<Vec<String>>();
				frame.render_widget(List::new(items_view), top);
				frame.render_widget(textarea.widget(), bottom);
			})
			.unwrap();
	}
}

fn get_key(key_tx: Arc<Sender<Event>>) {
	loop {
		let key = event::read().unwrap();
		match key {
			Event::Key(KeyEvent {
				code: KeyCode::Esc, ..
			}) => return,
			Event::Key(KeyEvent {
				code: KeyCode::Char('c'),
				modifiers: KeyModifiers::CONTROL,
				..
			}) => return,
			_ => (),
		}
		key_tx.send(key).unwrap();
	}
}

fn send_message(
	socket: Arc<Mutex<Client<Box<dyn NetworkStream + Send>>>>,
	rx: Arc<Mutex<Receiver<String>>>,
) {
	loop {
		sleep(Duration::from_millis(100));
		let Ok(message) = rx.lock().unwrap().try_recv() else {
			continue;
		};
		let mut socket = socket.lock().unwrap();
		socket.send_message(&Message::text(message)).unwrap();
	}
}

fn read_message(
	socket: Arc<Mutex<Client<Box<dyn NetworkStream + Send>>>>,
	rm_tx: Arc<Sender<String>>,
) {
	loop {
		sleep(Duration::from_millis(100));
		let s = Arc::clone(&socket);
		let mut socket = s.lock().unwrap();
		let Ok(message) = socket.recv_message() else {
			continue;
		};
		if let OwnedMessage::Text(text) = message {
			rm_tx.send(text).unwrap();
		}
	}
}

fn restore_terminal() -> Result<(), std::io::Error> {
	let is_raw = crossterm::terminal::is_raw_mode_enabled()?;
	if is_raw {
		crossterm::terminal::disable_raw_mode()?;
	}
	crossterm::execute!(std::io::stdout(), LeaveAlternateScreen, Show)?;
	Ok(())
}


fn main() {
	let Ok(address) = std::env::var("address") else {
		eprintln!("address variable not set");
		return;
	};

	let sm = StringChannel::new();
	let sm_tx = sm.sender;
	let sm_rx = sm.receiver;
	let rm = StringChannel::new();
	let rm_tx = rm.sender;
	let rm_rx = rm.receiver;
	let ke = EventChannel::new();
	let key_tx = ke.sender;
	let key_rx = ke.receiver;

	use websocket::client::builder::ClientBuilder;
	let socket: Arc<Mutex<Client<Box<dyn NetworkStream + Send>>>> = Arc::new(Mutex::new(
		ClientBuilder::new(&address).unwrap().connect(None).unwrap(),
	));
	socket.lock().unwrap().set_nonblocking(true).unwrap();

	let (t1_key_rx, t1_rm_rx, t1_sm_tx) =
		(Arc::clone(&key_rx), Arc::clone(&rm_rx), Arc::clone(&sm_tx));
	thread::spawn(|| spawn_tui(t1_key_rx, t1_rm_rx, t1_sm_tx));
	
	let (t2_s, t2_rm_tx) = (Arc::clone(&socket), Arc::clone(&rm_tx));
	thread::spawn(|| read_message(t2_s, t2_rm_tx));
	
	let (t3_s, t3_sm_rx) = (Arc::clone(&socket), Arc::clone(&sm_rx));
	thread::spawn(|| send_message(t3_s, t3_sm_rx));

	let t4_s = Arc::clone(&socket);
	thread::spawn(move || {
		let ping = DataFrame::new(true, Opcode::Ping, vec![]);
		loop {
			sleep(Duration::from_secs(30));
			let mut socket = t4_s.lock().unwrap();
			socket.send_dataframe(&ping).unwrap();
		}
	});

	get_key(key_tx);
	restore_terminal().unwrap();
}
